

## Что такое Git Bash?
 **Git Bash** - это командная оболочка (shell), которая предоставляет интерфейс командной строки для работы с системой контроля версий Git на операционных системах Windows, основанных на командной строке Git для Unix. Git Bash позволяет использовать командные сценарии (скрипты) для автоматизации задач в Git.

**Git Bash** обеспечивает пользователей Windows доступом к функциональности Git, позволяя им использовать те же команды и командные инструменты, что и пользователи Unix/Linux.

**Git Bash** обеспечивает полный набор команд Git, таких как:
 - клонирование репозитория
 - создание веток, добавление
 - коммит изменений
 - получение обновлений из удаленных репозиториев 
 - предоставляет доступ к Unix-утилитам, таким как ls, grep, sed и другим
 

## Что делает команда Git Pull?
Команда **Git Pull** используется для получения (принятия) последних изменений из удаленного репозитория Git и объединения их с текущей локальной веткой.

При выполнении **Git Pull** происходит следующее:
- Получение изменений: Git связывается с удаленным репозиторием, указанным в настройках вашего локального репозитория, и получает все последние изменения (коммиты), которых нет в вашей локальной ветке.
- Объединение изменений: После получения изменений Git пытается объединить их с текущей локальной веткой. Если изменения не конфликтуют с локальными изменениями, Git автоматически объединит их.
- Разрешение конфликтов (при необходимости): Если возникают конфликты - ситуации, когда Git не может автоматически объединить изменения из-за противоречий в коде, Git останавливается и указывает на места конфликта в файлах. Необходимо вручную разрешить эти конфликты, отредактировав соответствующие файлы.
- Создание нового коммита: После успешного объединения изменений Git создаст новый коммит, который объединяет последние изменения из удаленного репозитория с локальной веткой. Этот коммит будет иметь двух родителей - предыдущий коммит на ветке и последний коммит из удаленного репозитория.

Использование **Git Pull** удобно, когда вы хотите получить последние изменения из удаленного репозитория и объединить их с вашей локальной веткой перед тем, как внести свои изменения и отправить их обратно на удаленный репозиторий. Это помогает синхронизировать вашу работу с работой других разработчиков и обеспечивает актуальность вашего кода.


## Чем Git Pull отличается от Git Push?
Команды **Git Pull** и **Git Push** выполняют разные операции в Git:

**Git Pull**: 
- используется для получения последних изменений из удаленного репозитория и объединения их с вашей локальной веткой
- выполняет две операции: сначала получает (принимает) изменения из удаленного репозитория с помощью команды Git Fetch, а затем автоматически объединяет их с текущей локальной веткой
- если есть конфликты, они должны быть разрешены вручную.

**Git Push**: 
- используется для отправки локальных коммитов в удаленный репозиторий
- отправляет изменения, которые  сделали в  локальной ветке, на сервер, чтобы они были доступны другим разработчикам
- Git создает новый коммит на удаленном репозитории, который содержит  изменения
- может потребоваться указать имя удаленного репозитория и ветку, в которую необходимо выполнить отправку

Итак, основное отличие между **Git Pull** и **Git Push** заключается в направлении передачи изменений:
- **Git Pull** получает изменения из удаленного репозитория и объединяет их с вашей локальной веткой.
- **Git Push** отправляет ваши локальные изменения в удаленный репозиторий для совместного использования с другими разработчиками.
Оба этих шага - получение изменений и отправка изменений - важны для эффективной работы в команде и поддержания актуальности кода.


## Что такое Merge Request?
**Merge Request** (MR) - это механизм сотрудничества в системе контроля версий Git, который позволяет разработчикам предложить и объединить свои изменения (в виде коммитов) с основной веткой проекта. Он обычно используется в системах управления версиями, таких как GitLab, GitHub или Bitbucket, где команды разработчиков могут взаимодействовать и рецензировать код других участников проекта.

Когда разработчик завершает работу над своими изменениями в отдельной ветке (обычно названной "фича-веткой" или "веткой задачи"), он создает **Merge Request**, в котором он предлагает свои коммиты для объединения с основной веткой. Этот запрос включает изменения, описание задачи и комментарии или запросы на проверку кода (Code Review) со стороны других разработчиков.

Другие участники команды могут просматривать код, оставлять комментарии и предлагать изменения в рамках **Merge Request**. После того, как изменения прошли рецензию и все проблемы разрешены, ответственный за проект разработчик может объединить (мерджить) ветку с изменениями с основной веткой проекта.

**Merge Request** обеспечивает прозрачность, рецензирование и управление процессом интеграции кода. Он упрощает сотрудничество между разработчиками, позволяет проверять код, обсуждать изменения и устранять возможные проблемы до того, как они войдут в основную ветку. Это помогает улучшить качество кода и предотвратить конфликты при интеграции изменений в проекте.


## Чем между собой отличаются команды Git Status и Git Log?
Команды **Git Status** и **Git Log** выполняют разные функции в Git и предоставляют различную информацию о состоянии репозитория и истории коммитов.

**Git Status**: 
- позволяет узнать текущее состояние локального репозитория
- показывает, есть ли непроиндексированные изменения в файлах, изменения, готовые к коммиту, или файлы, которые не отслеживаются Git
- показывает текущую ветку и предлагает информацию о конфликтах слияния, если они есть
- предоставляет обзор состояния файлов и помогает принять решение о том, какие изменения нужно проиндексировать и коммитить

**Git Log**: 
- выводит историю коммитов в  репозитории
- показывает список коммитов, начиная с самого последнего, и отображает информацию о каждом коммите, такую как хэш коммита, автор, дата и сообщение коммита
-  может показывать графическое представление ветвления и слияния коммитов

Таким образом, основное отличие заключается в том, что **Git Status** сообщает текущее состояние рабочей директории и индекса (стейджа), позволяя принимать решения о фиксации изменений, а **Git Log** показывает историю коммитов, чтобы можно было изучить изменения, сделанные в проекте со временем.



## Что такое Submodule?
**Submodule** (подмодуль) в Git - это механизм, который позволяет включать один репозиторий Git внутри другого репозитория Git как подкаталог.

Подмодуль представляет собой отдельный репозиторий Git со своей собственной историей коммитов, ветками и файлами. Он связан с родительским репозиторием в определенном коммите, и при клонировании или обновлении родительского репозитория также можно обновить и подмодуль до определенной версии коммита.

Использование подмодулей в Git полезно в следующих случаях, например:

- внедрение внешних зависимостей: если  проект зависит от другого репозитория или компонента, можно добавить его как подмодуль, чтобы включить его в свой проект. Это может быть сторонняя библиотека или инструмент, которые  проект использует.
- разделение проекта на модули: если проект состоит из нескольких независимых компонентов, каждый из которых имеет свою собственную разработку и историю коммитов, можно организовать каждый компонент как подмодуль и управлять ими независимо.
- управление версиями: подмодули позволяют контролировать и отслеживать версии зависимостей, можно указать определенную версию коммита подмодуля, чтобы обеспечить согласованность и воспроизводимость среды разработки.

Подмодули позволяют эффективно управлять зависимостями и разделением проекта на модули, обеспечивая удобство и гибкость при разработке проектов, состоящих из нескольких компонентов.


## С помощью какой команды можно добавлять сабмодули в свой репозиторий?
Для добавления подмодулей в свой репозиторий в Git можно использовать команду **"Git Submodule Add"**. 

Пример:
git submodule add <URL репозитория> <путь_к_подмодулю>
<URL репозитория>: URL удаленного репозитория, который нужно добавить в качестве подмодуля.
<путь_к_подмодулю>: путь, по которому необходимо добавить подмодуль в репозитории. Это может быть относительный или абсолютный путь к каталогу в проекте.

После выполнения команды **"Git Submodule Add"**, Git выполнит несколько действий:
- клонирует указанный репозиторий по указанному URL в подкаталог репозитория
- создает специальный коммит, известный как коммит "superproject", который указывает на добавленный подмодуль
- добавляет запись о подмодуле в файл ".gitmodules" в корне репозитория, этот файл содержит информацию о добавленных подмодулях
- после добавления подмодуля в репозиторий может потребоваться выполнить git commit и git push для сохранения изменений в репозитории

После клонирования репозитория, который содержит подмодули, необходимо выполнить "git submodule init" и "git submodule update", чтобы получить содержимое подмодулей. Это нужно для того, чтобы убедиться, что подмодули инициализированы и обновлены до правильных версий.